﻿#pragma once

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#pragma comment(lib,"winmm.lib")

#define WINCLASSNAME "win32gui" //应用程序名称
LRESULT WINAPI WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ); //声明窗口函数

class win32gui
{
public:
	win32gui(void);
	~win32gui(void);

	bool RegWnd(UINT Icon=0,UINT cur=0);//注册窗口
	bool CreateWnd(LPCTSTR title,HINSTANCE PrevInst,bool isFull);//创建窗口
	bool Run();//消息循环
	void DeleteWnd();//结束应用程序

private:
	WNDCLASSEX wc;
	MSG m_msg;
	int m_width;
	int m_height;
	HWND m_hWnd;

	HINSTANCE hInstance;

};

