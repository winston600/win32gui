﻿#include "win32gui.h"


win32gui::win32gui(void)
{
	hInstance = GetModuleHandle(NULL);//获取实例句柄
}

win32gui::~win32gui(void)
{
}

//-----------------------------------------------------------------------
//注册窗口
//----------------------------------------------------------------------
bool win32gui::RegWnd(UINT Icon,UINT cur)
{
	// 定义一个Windows类，指定消息的处理函数为MsgProc
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = wc.cbWndExtra = NULL;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(COLOR_WINDOW+1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = TEXT("win32gui");
	wc.hIcon = wc.hIconSm = LoadIcon(NULL,IDI_APPLICATION); 
	wc.hCursor = LoadCursor(NULL,IDC_ARROW); 

	if(cur)  
		wc.hCursor=LoadCursor(NULL,IDC_ARROW);     //定义鼠标图标
 
	// 注册这个窗口类
	RegisterClassEx( &wc );

	return true;
}
//-------------------------------------------------------------------------------------------
//创建窗口
//-------------------------------------------------------------------------------------------
bool win32gui::CreateWnd(LPCTSTR title,HINSTANCE PrevInst,bool isFull)
{
	if(PrevInst) 
		return false;                  //判断是否已经有相同的应用实体在运行

	// 创建应用程序窗口
	m_hWnd = CreateWindowEx(NULL,
							wc.lpszClassName,
							wc.lpszClassName,
							WS_OVERLAPPEDWINDOW,
							CW_USEDEFAULT,
							CW_USEDEFAULT,
							CW_USEDEFAULT,
							CW_USEDEFAULT,
							NULL,
							NULL,
							wc.hInstance,
							NULL );//创建窗口

	if(!m_hWnd)  
		return false;
 
	// 显示窗口
	ShowWindow( m_hWnd, SW_SHOW );
	UpdateWindow( m_hWnd );

	return true;
}
//----------------------------------------------------------------------------------------------
//运行，消息循环
//----------------------------------------------------------------------------------------------
bool win32gui::Run()
{      
	if( m_msg.message != WM_QUIT)
	{
		////进入消息循环
		while(true)
		{
			if(!GetMessage(&m_msg,0,0,0))
				break;
			TranslateMessage(&m_msg);
			DispatchMessage(&m_msg);
		}
		return true;
	}
	else 
		return false; 
}
//-------------------------------------------------------------------------------
// 注销窗口类
//--------------------------------------------------------------------------------
void win32gui::DeleteWnd()
{ 
     UnregisterClass(TEXT("win32gui"), wc.hInstance );
}
//窗口过程回调函数，在这里处理窗口消息
LRESULT WINAPI WndProc( HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(NULL);
		break;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
 
